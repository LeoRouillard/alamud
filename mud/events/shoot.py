from .event import Event2, Event3

class ShootEvent(Event2):
    NAME = "shoot"

    def perform(self):
        if not self.object.has_prop("shootable") or self.object.has_prop("shootable-with"):
            self.fail()
            return self.inform("shoot.failed")
        self.inform("shoot")

class ShootWithEvent(Event3):
    NAME = "shoot-with"

    def perform(self):
        if not (self.object.has_prop("shootable") and self.object2.has_prop("projectile")):
            self.fail()
            return self.inform("shoot.failed")
        self.object2.move_to(None)
        self.inform("shoot-with")
