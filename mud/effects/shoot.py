from .effect import Effect2, Effect3
from mud.events import ShootEvent

class ShootEffect(Effect2):
    EVENT = ShootEvent
